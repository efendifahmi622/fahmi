import React, {useState} from 'react'
import { View,Text, Image,StyleSheet,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Tengah = () => {
    const [state, setstate] = useState(0)
    return (
        <View>
        <View style={tengah.coba}>
         <Image style={{width:35,height:35,borderRadius:20,marginVertical:10,marginHorizontal:10}} source={require('../assets/2.jpg')}/>
         <Text style={{fontWeight:'bold',marginVertical:17,marginRight:30}}>Lilis Amanda</Text> 
         <Icon style={tengah.Icon} color="gray" name="ellipsis-vertical" size={25}/>
         </View>
        <Image source={require('../assets/3.jpg')} style={{width:358,height:240}}/>
        <View style={tengah.logo}>
            <TouchableOpacity>
            <Icon style={tengah.navItem} color="gray" name="heart" size={30} onPress={() => setstate(state + 1)}/>
            </TouchableOpacity>
            <Icon style={tengah.navItem} color="gray" name="chatbubble-ellipses" size={30}/>
            <Icon style={tengah.navItem} color="gray" name="paper-plane" size={30}/>
            <Icon style={tengah.nav} color="gray" name="bookmarks" size={30}/>
            </View>
            <View style={tengah.text}>
            <Text style={{fontWeight:'bold',marginHorizontal:5}}>{state} Suka</Text>
            <Text style={{marginHorizontal:5}}><Text style={{fontWeight:'bold',marginHorizontal:5}}>Lilis Amanda</Text>. Cewek cantik di edentik dengan ke indahan yang sangat mempesona</Text>
            </View>
        </View>

    )
}

const tengah= StyleSheet. create({
    coba: {
        flexDirection:'row',
        backgroundColor:'white',
        elevation:3
    },
    Icon: {
        marginHorizontal:160,
        marginVertical:10   
    },
    logo: {
        height:100,
        flexDirection:'row',
        marginHorizontal:5,
        marginVertical:10,
        
    },
    navItem: {
        marginHorizontal:5,
    },
    nav: {
        marginHorizontal:145
    },
    text: {
        height:100,
        backgroundColor:'white',
        flexDirection:'column',
    },
})
export default Tengah
