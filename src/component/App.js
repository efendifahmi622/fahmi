import React from 'react'
import { View, ScrollView } from 'react-native'
import { Atas,Status, Tengah,T1,T2,T3,T4 } from './index'

const App = () => {
  return (
    <View>
     <Atas />
     <ScrollView>
       <Status />
       <Tengah />
       <T1/>
       <T2/>
       <T3/>
       <T4/>
     </ScrollView>
    </View>
  )
}

export default App
